﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Runtime.InteropServices;


namespace WindowsFormsApplication1
{
    
    public partial class mainpage: Form 
    {
        static System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        //Capture CapWebCam;
        Seq<Point> Hull;
        public Image<Bgr, byte> imgOrignal;
        Seq<MCvConvexityDefect> defects;
        MCvConvexityDefect[] defectArray;
        MCvBox2D box;
        MemStorage storage = new MemStorage();
  
        bool button_pressed = false;

        //double threshold = 0;
        int i = 0, j = 0;
        Image<Bgr, Byte> bg;
        private Capture capture;        //takes images from camera as image frames
        private Capture bgframe;
        private bool captureInProgress; // checks if capture is executing
        double threshold = 0;
       // private HaarCascade haarCascade = new HaarCascade(@"C:\Users\SHUBHANKAR\documents\visual studio 2013\Projects\WindowsFormsApplication1\WindowsFormsApplication1\haarcascade_frontalface_default.xml");


        Image<Bgr, byte> forcanny;
        Image<Gray, byte> gcanny;



        Image<Bgr, Byte> currentFrame;
        Image<Bgr, Byte> currentFrameCopy;
        Image<Bgr, byte> afterfr;
        Capture g;
        AdaptiveSkinDetector detector;

        int frameWidth;
        int frameHeight;

        Hsv hsv_min;
        Hsv hsv_max;
        Ycc YCrCb_min;
        Ycc YCrCb_max;

        Seq<Point> hull;
        Seq<Point> filteredHull;
        Seq<MCvConvexityDefect> defects1;
        MCvConvexityDefect[] defectArray1;
        Rectangle handRect;
        MCvBox2D box1;
        Ellipse ellip;

        Image<Bgr, byte> img;
        Image<Ycc, Byte> sub;
        Boolean calib_flag =false;
        CircleF maxin;
        double top=0,left=0,right=0;
        public mainpage()
        {
            InitializeComponent();
            MessageBox.Show("Please fill a value in the textbox before pressing start. Normal values between 1 and 4");
            detector = new AdaptiveSkinDetector(1, AdaptiveSkinDetector.MorphingMethod.NONE);
            hsv_min = new Hsv(0, 45, 0);
            hsv_max = new Hsv(20, 255, 255);
            YCrCb_min = new Ycc(50, 50,50);
            YCrCb_max = new Ycc(200, 200, 200);
            box1 = new MCvBox2D();
            ellip = new Ellipse();
            trackBar1.Value = 0;
             trackBar2.Value=255;
             trackBar3.Value=131;
             trackBar4.Value=185;
             trackBar5.Value=80;
             trackBar6.Value=135;

           
            
        }

        public partial class NativeMethods
         {
            /// Return Type: BOOL->int  
            ///X: int  
            ///Y: int  
            [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "SetCursorPos")]
            [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)]
            public static extern bool SetCursorPos(int X, int Y);

        }
        

            /*
        public Image<Bgr,Byte> dowork1()
            {
                Image<Bgr, Byte> currentFrame =imgOrignal.Convert<Bgr,byte>();

                if (currentFrame != null)
                {
                    Image<Gray, Byte> grayFrame = currentFrame.Convert<Gray, Byte>();

                    var detectedFaces = grayFrame.DetectHaarCascade(haarCascade)[0];
                    
                    foreach (var face in detectedFaces)
                    {
                        currentFrame.Draw(face.rect, new Bgr(0, 0, 0), -15);
                        
                    }
                    
                    
                }
                return currentFrame;

            }
*/
        private void ProcessFrame(object sender, EventArgs arg)
        {
            int Finger_num = 0;
            Double Result1 = 0;
            Double Result2 = 0;
            //Background subtraction
            bg = bgframe.QueryFrame();
            //    iborignal.Image = bg;
            ++j;
            // max2TextBox.Text = j.ToString();
            //max2TextBox.Text = capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_CONTRAST).ToString();
            //CvInvoke.cvSetCaptureProperty(capture.Ptr, CAP_PROP.CV_CAP_PROP_CONTRAST, 0);

            imgOrignal = capture.QueryFrame();
            PointF calib = new PointF(15, 20);
            CircleF calibrate = new CircleF(calib, 7f);

            imgOrignal.Draw(calibrate, new Bgr(50, 50, 100), 2);


        
                Image<Bgr, Byte> currentFrame = imgOrignal.Convert<Bgr, byte>();
                imgOrignal._EqualizeHist();
           
                   for (i = 0; i < int.Parse(max2TextBox.Text); i++)
                    {
                        imgOrignal._EqualizeHist();
                        //imgOrignal._EqualizeHist();
                        imgOrignal._GammaCorrect(1.8d);
                    }
                
              
                //imageBox1.Image = imgOrignal;
                //face detection function call
                /**/
                /*   if (j % 3 == 0)
                   {
                       Task t1 = Task.Factory.StartNew<Image<Bgr, byte>>(dowork1).ContinueWith(t =>
                       {
                           forcanny = t.Result; imageBox1.Image = forcanny;
                           //            gcanny = forcanny.Convert<Gray, byte>();
                           //  gcanny = gcanny.Add(res2.Convert<Gray, byte>(), gcanny);

                           //          gcanny = imgOrignal.Canny(60, 230);
                           //       imageBox2.Image = gcanny;
                       });
                       if (j == 35000) j = 0;
                   }
                   */
                double threshold = double.Parse(max2TextBox.Text);
                Image<Gray, Byte> gray = imgOrignal.Convert<Gray, Byte>();

                //convert to binary image using the threshold
                gray = gray.ThresholdBinary(new Gray(threshold), new Gray(255));

                // copy pixels from the original image where pixels in 
                // mask image is nonzero
                Image<Bgr, Byte> newimg = imgOrignal.Copy(gray);
                imageBox2.Image = newimg;
                afterfr = newimg;

                Image<Ycc, Byte> currentYCrCbFrame = afterfr.Convert<Ycc, byte>();
                Image<Gray, byte> skin = new Image<Gray, byte>(afterfr.Width, afterfr.Height);

                int a, b, c, d, e, f;
                a = trackBar1.Value;
                b = trackBar2.Value;
                c = trackBar3.Value;
                d = trackBar4.Value;
                e = trackBar5.Value;
                f = trackBar6.Value;
                skin = currentYCrCbFrame.InRange(new Ycc(a, c, e), new Ycc(b, d, f));
                //            
                StructuringElementEx rect_12 =
          new StructuringElementEx(5, 5, 3, 3, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_RECT);


                //Eroding the source image using the specified structuring element
                CvInvoke.cvErode(skin, skin, rect_12, 1);

                StructuringElementEx rect_6 =
                  new StructuringElementEx(6, 6, 3, 3, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_RECT);

                //dilating the source image using the specified structuring element
                CvInvoke.cvDilate(skin, skin, rect_6, 2);
                //smoothing the filterd , eroded and dilated image.
                skin = skin.SmoothGaussian(15);
                skin.Flip(FLIP.HORIZONTAL);
            iborignal.Image = skin;


                Contour<Point> contours = skin.FindContours(); //extracting all contours.
                Contour<Point> biggestContour = null;

                //extracting the biggest contour.
                while (contours != null)
                {
                    Result1 = contours.Area;
                    if (Result1 > Result2)
                    {
                        Result2 = Result1;
                        biggestContour = contours;
                    }
                    contours = contours.HNext;
                }

                if (biggestContour != null)
                {
                    Finger_num = 0;

                    biggestContour = biggestContour.ApproxPoly((1));
                    currentFrame.Draw(biggestContour, new Bgr(Color.Black), 2);

                    Hull = biggestContour.GetConvexHull(ORIENTATION.CV_CLOCKWISE);
                    defects = biggestContour.GetConvexityDefacts(storage, ORIENTATION.CV_CLOCKWISE);
                    currentFrame.DrawPolyline(Hull.ToArray(), true, new Bgr(0, 0, 256), 2);

                    box = biggestContour.GetMinAreaRect();
                   
                    defectArray = defects.ToArray();
                    left = right = top = 0;
                    for (int i = 0; i < defects.Total; i++)
                    {
                        if (defectArray[i].StartPoint.Y < box.center.Y)
                        {
                            PointF startPoint = new PointF((float)defectArray[i].StartPoint.X,
                                                        (float)defectArray[i].StartPoint.Y);

                            PointF depthPoint = new PointF((float)defectArray[i].DepthPoint.X,
                                                            (float)defectArray[i].DepthPoint.Y);

                            PointF endPoint = new PointF((float)defectArray[i].EndPoint.X,
                                                            (float)defectArray[i].EndPoint.Y);


                            
                            CircleF startCircle = new CircleF(startPoint, 5f);
                            CircleF depthCircle = new CircleF(depthPoint, 5f);
                            CircleF endCircle = new CircleF(endPoint, 5f);

                            float x2=startCircle.Center.X;
                            float x1=depthCircle.Center.X;
                            float y2=startCircle.Center.Y;
                            float y1=depthCircle.Center.Y;
                            float x3=endCircle.Center.X;
                            float y3=endCircle.Center.Y;
                            double d12=Math.Sqrt(Math.Pow((int)(x1 - x2), 2) +
                                               Math.Pow((int)(y1 - y2), 2));
                            double d23=Math.Sqrt(Math.Pow((int)(x2 - x3), 2) +
                                               Math.Pow((int)(y2 - y3), 2));
                            
                            double d13=Math.Sqrt(Math.Pow((int)(x1 - x3), 2) +
                                               Math.Pow((int)(y1 - y3), 2));
                           double ang = Math.Acos((Math.Pow(d12, 2) + Math.Pow(d13, 2) - Math.Pow(d23, 2)) / (2 * Math.Pow(d12, 2) * Math.Pow(d13, 2)));
                         

                            currentFrame.Draw(startCircle, new Bgr(100, 0, 0), 3);
                            currentFrame.Draw(endCircle, new Bgr(0, 100, 0), 3);
                            currentFrame.Draw(depthCircle, new Bgr(0, 0, 100), 3);
                            int p;
                            if ((startCircle.Center.Y < box.center.Y || depthCircle.Center.Y < box.center.Y) &&
                                    (startCircle.Center.Y < depthCircle.Center.Y) &&
                                    (Math.Sqrt(Math.Pow(startCircle.Center.X - depthCircle.Center.X, 2) +
                                               Math.Pow(startCircle.Center.Y - depthCircle.Center.Y, 2)) >
                                               box.size.Height / 6.5) )
                            {
                                if (startCircle.Center.Y < top || top == 0)
                                { top = startCircle.Center.Y; p = i; }
                                if (startCircle.Center.X < left || left == 0)
                                    left = startCircle.Center.X;
                                if (startCircle.Center.X > right || right == 0)
                                    right = startCircle.Center.X;
                                
                                Finger_num++;
                            }
                           
                            //Rectangle roi = new Rectangle((int)left - 5, (int)top + 5, (int)right - (int)left + 30, (int)(depthCircle.Center.Y - startCircle.Center.Y) * 2);
                            //currentFrame.Draw(roi, new Bgr(150, 150, 150), 2);
                           // currentFrame.ROI = roi;
                            imageBox1.Image = currentFrame;
                            
                        }
                    }

                    //label2.Text = Finger_num.ToString();            // updating finger count
                    //find center of countour
                    MCvMoments moment = new MCvMoments();               // a new MCvMoments object

                    moment = biggestContour.GetMoments();           // Moments of biggestContour

                    CvInvoke.cvMoments(biggestContour, ref moment, 0);

                    double m_00 = CvInvoke.cvGetSpatialMoment(ref moment, 0, 0);
                    double m_10 = CvInvoke.cvGetSpatialMoment(ref moment, 1, 0);
                    double m_01 = CvInvoke.cvGetSpatialMoment(ref moment, 0, 1);
                    int current_X = Convert.ToInt32(m_10 / m_00) ;      // X location of centre of contour              
                    int current_Y = Convert.ToInt32(m_01 / m_00) ;      // Y location of center of contour

                                        
                    
                    if (Finger_num ==1 ||Finger_num==0 )
                    {
                        
                        Cursor.Position = new Point(current_X , current_Y );
                        label2.Text = "number of fingers detected="+Finger_num.ToString() + "\n" + "Position of cursor" + current_X.ToString() + ",\n" + current_Y.ToString();
                    }
                    if (Finger_num >= 4)
                    {
                        DoMouseClick();                     // function clicks mouse left button
                    }  
                    
                }


                // imageBox1.Image = imgOrignal;

            
        
          
           
               
            
        }
        [DllImport("user32.dll")]
        static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint dwData,
           int dwExtraInfo);

        private const int MOUSEEVENTF_LEFTDOWN = 0x02;          // mouse left button pressed 
        private const int MOUSEEVENTF_LEFTUP = 0x04;            // mouse left button unpressed
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;         // mouse right button pressed
        private const int MOUSEEVENTF_RIGHTUP = 0x10;           // mouse right button unpressed

        //this function will click the mouse using the parameters assigned to it
        public void DoMouseClick()
        {
            //Call the imported function with the cursor's current position
            uint X = Convert.ToUInt32(Cursor.Position.X);
            uint Y = Convert.ToUInt32(Cursor.Position.Y);
            mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, X, Y, 0, 0);
       
        }   
        private void btmPauseOrResum_Click(object sender, EventArgs e)
        {
            #region if capture is not created, create it now
            if (capture == null)
            {
                try
                {
            
                    capture = new Capture();
           
                    bgframe = new Capture();
                }
                catch (NullReferenceException excpt)
                {
                    MessageBox.Show(excpt.Message);
                }
            }
            #endregion

            if (capture != null)
            {
                if (captureInProgress)
                {  //if camera is getting frames then stop the capture and set button Text
                    // "Start" for resuming capture
                    btmPauseOrResum.Text = "Start!"; //
                    Application.Idle -= ProcessFrame;
                }
                else
                {
                    //if camera is NOT getting frames then start the capture and set button
                    // Text to "Stop" for pausing capture


                    btmPauseOrResum.Text = "Stop";
                    Application.Idle += ProcessFrame;
                }

                captureInProgress = !captureInProgress;
            }
           
        }

        private void claib_Click(object sender, EventArgs e)
        {
            calib_flag = true;
        }

        private void max2TextBox_TextChanged(object sender, EventArgs e)
        {

        }

      
    
}
    }

/*           frameWidth = capture.Width;
            frameHeight = capture.Height;
            max2TextBox.Text = trackBar1.Value.ToString(); 
            // load the threshold value for grayscale image
    
     // create new image
     Image<Bgr, Byte> img = imgOrignal;
 
      //convert to grayscale
      Image<Gray, Byte> gray = img.Convert<Gray, Byte>();
 
      //convert to binary image using the threshold
      gray = gray.ThresholdBinary(new Gray(threshold), new Gray(255));
 
      // copy pixels from the original image where pixels in 
      // mask image is nonzero
      Image<Ycc, Byte> sub = img.Copy(gray).Convert<Ycc,byte>();
      sub = bg.Convert<Ycc,byte>().AbsDiff(sub); 
      // display result
      //if (displayResult) this.NewImage("Background segmented", newimg);
           // sub = imgOrignal.Convert<Ycc, Byte>();
            //Image<Ycc, Byte> subbg = bg.Convert<Ycc, Byte>();
            //sub = sub.AbsDiff(subbg);
            iborignal.Image = sub.Convert<Bgr,byte>();
            //imageBox1.Image = imgOrignal;
            //Background subtraction done
            //split image in y, cb and cr channels
            /*Image<Gray, Byte>[] channels = sub.Split();


            Image<Gray, Byte> y = channels[0];
            Image<Gray, Byte> cb = channels[1];
            Image<Gray, Byte> cr = channels[2];

            // applying filters to remove noise
            Image<Gray, byte> yfilter = y.InRange(new Gray(30), new Gray(150));
            Image<Gray, byte> cbfilter = cb.InRange(new Gray(8.5), new Gray(150));
            Image<Gray, byte> crfilter = cr.InRange(new Gray(6.5), new Gray(50));
            //  Image<Gray, byte> yfilter = y.ThresholdBinary(new Gray(20), new Gray(150));
            //Image<Gray, byte> cbfilter = cb.ThresholdBinary(new Gray(7), new Gray(150));
            //Image<Gray, byte> crfilter = cr.ThresholdBinary(new Gray(7), new Gray(50));

            Image<Gray, byte> res = null;
            StructuringElementEx rect_12 = new StructuringElementEx(4, 4, 3, 3, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_RECT);

            //Eroding the source image using the specified structuring element
            CvInvoke.cvErode(yfilter, yfilter, rect_12, 1);
            CvInvoke.cvErode(crfilter, crfilter, rect_12, 1);
            CvInvoke.cvErode(cbfilter, cbfilter, rect_12, 1);


            StructuringElementEx rect_6 =
              new StructuringElementEx(4, 4, 3, 3, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_RECT);

            //dilating the source image using the specified structuring element
            CvInvoke.cvDilate(yfilter, yfilter, rect_6, 2);
            CvInvoke.cvDilate(crfilter, crfilter, rect_6, 2);
            CvInvoke.cvDilate(cbfilter, cbfilter, rect_6, 2);

            //Adding 3 channels
            res = yfilter.Add(crfilter, cbfilter);
            Image<Ycc, byte> res2;
            res2 = res.Convert<Ycc, byte>();
            //adding mask of original ycrcb frame
            res2 = res2.And(sub);
            CvInvoke.cvErode(res2, res2, rect_12, 1);
            iborignal.Image = res2.Convert<Bgr, byte>();
            imageBox1.Image = cbfilter;
            imageBox2.Image = crfilter;
*/