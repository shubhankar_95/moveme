﻿namespace WindowsFormsApplication1
{
    partial class mainpage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btmPauseOrResum = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.iborignal = new Emgu.CV.UI.ImageBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.max2TextBox = new System.Windows.Forms.TextBox();
            this.imageBox2 = new Emgu.CV.UI.ImageBox();
            this.imageBox1 = new Emgu.CV.UI.ImageBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.trackBar3 = new System.Windows.Forms.TrackBar();
            this.trackBar4 = new System.Windows.Forms.TrackBar();
            this.trackBar5 = new System.Windows.Forms.TrackBar();
            this.trackBar6 = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.iborignal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar6)).BeginInit();
            this.SuspendLayout();
            // 
            // btmPauseOrResum
            // 
            this.btmPauseOrResum.Location = new System.Drawing.Point(1009, 420);
            this.btmPauseOrResum.Name = "btmPauseOrResum";
            this.btmPauseOrResum.Size = new System.Drawing.Size(75, 23);
            this.btmPauseOrResum.TabIndex = 0;
            this.btmPauseOrResum.Text = "Start!";
            this.btmPauseOrResum.UseVisualStyleBackColor = true;
            this.btmPauseOrResum.Click += new System.EventHandler(this.btmPauseOrResum_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1006, 459);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Number of fingers detected=";
            // 
            // iborignal
            // 
            this.iborignal.Location = new System.Drawing.Point(-2, -2);
            this.iborignal.Name = "iborignal";
            this.iborignal.Size = new System.Drawing.Size(504, 403);
            this.iborignal.TabIndex = 2;
            this.iborignal.TabStop = false;
            // 
            // max2TextBox
            // 
            this.max2TextBox.Location = new System.Drawing.Point(1120, 423);
            this.max2TextBox.Name = "max2TextBox";
            this.max2TextBox.Size = new System.Drawing.Size(100, 20);
            this.max2TextBox.TabIndex = 3;
            this.max2TextBox.TextChanged += new System.EventHandler(this.max2TextBox_TextChanged);
            // 
            // imageBox2
            // 
            this.imageBox2.Location = new System.Drawing.Point(372, 407);
            this.imageBox2.Name = "imageBox2";
            this.imageBox2.Size = new System.Drawing.Size(623, 325);
            this.imageBox2.TabIndex = 2;
            this.imageBox2.TabStop = false;
            // 
            // imageBox1
            // 
            this.imageBox1.Location = new System.Drawing.Point(519, -2);
            this.imageBox1.Name = "imageBox1";
            this.imageBox1.Size = new System.Drawing.Size(671, 410);
            this.imageBox1.TabIndex = 2;
            this.imageBox1.TabStop = false;
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(992, 512);
            this.trackBar1.Maximum = 255;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(290, 45);
            this.trackBar1.TabIndex = 4;
            // 
            // trackBar2
            // 
            this.trackBar2.Location = new System.Drawing.Point(993, 541);
            this.trackBar2.Maximum = 255;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(289, 45);
            this.trackBar2.TabIndex = 5;
            // 
            // trackBar3
            // 
            this.trackBar3.Location = new System.Drawing.Point(993, 572);
            this.trackBar3.Maximum = 255;
            this.trackBar3.Name = "trackBar3";
            this.trackBar3.Size = new System.Drawing.Size(289, 45);
            this.trackBar3.TabIndex = 6;
            // 
            // trackBar4
            // 
            this.trackBar4.Location = new System.Drawing.Point(992, 603);
            this.trackBar4.Maximum = 255;
            this.trackBar4.Name = "trackBar4";
            this.trackBar4.Size = new System.Drawing.Size(290, 45);
            this.trackBar4.TabIndex = 7;
            // 
            // trackBar5
            // 
            this.trackBar5.Location = new System.Drawing.Point(993, 631);
            this.trackBar5.Maximum = 255;
            this.trackBar5.Name = "trackBar5";
            this.trackBar5.Size = new System.Drawing.Size(289, 45);
            this.trackBar5.TabIndex = 8;
            // 
            // trackBar6
            // 
            this.trackBar6.Location = new System.Drawing.Point(993, 665);
            this.trackBar6.Maximum = 255;
            this.trackBar6.Name = "trackBar6";
            this.trackBar6.Size = new System.Drawing.Size(289, 45);
            this.trackBar6.TabIndex = 9;
            // 
            // mainpage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1303, 732);
            this.Controls.Add(this.trackBar6);
            this.Controls.Add(this.trackBar5);
            this.Controls.Add(this.trackBar4);
            this.Controls.Add(this.trackBar3);
            this.Controls.Add(this.trackBar2);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.imageBox1);
            this.Controls.Add(this.imageBox2);
            this.Controls.Add(this.max2TextBox);
            this.Controls.Add(this.iborignal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btmPauseOrResum);
            this.Name = "mainpage";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.iborignal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btmPauseOrResum;
        private System.Windows.Forms.Label label2;
        private Emgu.CV.UI.ImageBox iborignal;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TextBox max2TextBox;
        private Emgu.CV.UI.ImageBox imageBox2;
        private Emgu.CV.UI.ImageBox imageBox1;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.TrackBar trackBar3;
        private System.Windows.Forms.TrackBar trackBar4;
        private System.Windows.Forms.TrackBar trackBar5;
        private System.Windows.Forms.TrackBar trackBar6;
    }
}

